# AIOps

Idea inspired by [this image](http://www.moogsoft.com/wp-content/uploads/2017/05/clusters_internal.jpg) I found whilst Googling.

## Viewing

### Live Demo

[https://www.jamesbillot.com/data-vis/aiops](https://www.jamesbillot.com/data-vis/aiops)

### Locally

Requires `http-server` or other localhost server.

```
# From the root of the project (where index.html is)
http-server .
```

Open your browser to [http://localhost:8080/](http://localhost:8080/)


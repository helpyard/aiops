import vue from 'rollup-plugin-vue';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'src/main.js',
  output: {
    file: 'dist/build.js',
    format: 'iife',
    sourcemap: true
  },
  plugins: [
    commonjs(),
    vue()
  ]
}

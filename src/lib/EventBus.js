import {enumify} from './utils.js';


export function EventBus(event_list) {

    this.E = enumify(event_list);

    this.bus = {};
    this.events_for_frame = [];
    this.events_payloads_for_frame = [];
    this.events_for_next_frame = [];
    this.events_payloads_for_next_frame = [];

    this.registerEventListener = (event_name, fn) => {
        if(this.E[event_name] === undefined) throw new Error('Unknown event: ' + event_name);
        if(this.bus[event_name] === undefined) {
            this.bus[event_name] = [fn];
        }
        else {
            this.bus[event_name].push(fn);
        }
    };

    this.deregisterEventListener = (event_name, fn) => {
        // @TODO
        throw new Error('deregisterEventListener is unimplemented');
    };

    this.pushEventForFrame = (event_name, payload) => {
        if(this.E[event_name] === undefined) throw new Error('Unknown event: ' + event_name);
        this.events_for_frame.push(event_name);
        this.events_payloads_for_frame.push(payload);  // NB can be undefined
    };

    this.pushEventForNextFrame = (event_name, payload) => {
        if(this.E[event_name] === undefined) throw new Error('Unknown event: ' + event_name);
        this.events_for_next_frame.push(event_name);
        this.events_payloads_for_next_frame.push(payload);  // NB can be undefined
    };

    this.tick = () => {
        // Process Events
        // NOTE(james): using a while loop with shift() as listeners can cause additional events to
        // be pushed onto queue this frame - so have to handle 'instant' newcomers
        let event_name, payload, listeners, i, n;
        while (event_name = this.events_for_frame.shift(), event_name !== undefined) {
            payload = this.events_payloads_for_frame.shift();
            listeners = this.bus[event_name];
            if(listeners !== undefined) {
                for(i = 0, n = listeners.length; i < n; i++) {
                    listeners[i](payload);
                }
            }
        }

        // Swap event buffer for next frame into current frame, ready for processing next frame
        this.events_for_frame = this.events_for_next_frame;
        this.events_for_next_frame = [];
        this.events_payloads_for_frame = this.events_payloads_for_next_frame;
        this.events_payloads_for_next_frame = [];
    };

}

import {oneAtRandomFrom, range} from "./utils";


export const MAX_SITUATIONS = 5;
export const MAX_EVENT_SOURCES = 25;
export const MAX_SITUATION_DURATION_MS = 1000 * 25;


// Generate a stream of Events to mimic ingestion events
export function EventSim(DataBus) {

    this.current_situations = [];
    this.launch_situation_next_frame = false;
    this.situation_id = 0;

    this.getNextSituationId = () => {
        this.situation_id++;
        return this.situation_id;
    };

    this.createSituation = ms => {
        if(this.current_situations.length < MAX_SITUATIONS) {
            this.current_situations.push({
                id: this.getNextSituationId(),
                ms_start: ms,
                ms_duration: MAX_SITUATION_DURATION_MS,
                ms_finish: ms + MAX_SITUATION_DURATION_MS
            });
        }
    };

    this.generateEvent = (situation, ms) => {
        // @STUB
        if(Math.random() < 0.25) {
            DataBus.pushEventForFrame('sim event fired', {
                situation_id: situation.id,
                type: oneAtRandomFrom(range(1, MAX_EVENT_SOURCES))
            });
        }
    };

    this.tick = ms => {
        // Launch new situations if requested
        if(this.launch_situation_next_frame) {
            this.createSituation(ms);
            this.launch_situation_next_frame = false;
        }

        // Filter out all situations that have run their course
        this.current_situations = this.current_situations.filter(situation => {
            return situation.ms_finish > ms;
        });

        // Generate events for the remaining situations
        let i, n;
        for(i = 0, n = this.current_situations.length; i < n; i++) {
            this.generateEvent(this.current_situations[i], ms);
        }
    };

    DataBus.registerEventListener('launch situation clicked', payload => {
        this.launch_situation_next_frame = true;
    });

}

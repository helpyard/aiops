import {isUndefined, range} from "./utils";

const MS_MAX_TIME_SINCE_LAST_SEEN = 10000;
const MAX_CLUSTER_BUCKET_INDEX = 6;

export function SituationManager(DataBus, STATE) {
    // NOTE(james): have to do all mutations directly on the STATE object. Aliasing it to something like this.live_events
    // means that Vue doesn't make it into an observable, but, doing it directly on STATE it works fine.
    STATE.live_events = [];
    STATE.live_situations = [];
    STATE.cluster_buckets = range(1, 7);

    this.new_event_queue = [];
    this.dupe_lookup = {};
    this.cluster_position_lookup = {};
    this.situation_last_seen = {};
    this.cluster_bucket_index = -1;

    this.updateSituationLastSeen = (situation_id, ms) => {
        this.situation_last_seen[situation_id] = ms;
    };

    this.isDupe = ev => {
        let key = '' + ev.situation_id + ev.type;
        if(isUndefined(this.dupe_lookup[key])) {
            this.dupe_lookup[key] = true;
            return false;
        }
        return true;
    };

    this.assignSituationToClusterBucket = situation_id => {
        if(isUndefined(this.situation_last_seen[situation_id])) {
            this.cluster_bucket_index++;
            if(this.cluster_bucket_index > MAX_CLUSTER_BUCKET_INDEX) {
                this.cluster_bucket_index = 0;
            }
            STATE.cluster_buckets[this.cluster_bucket_index] = situation_id;
        }
    };

    this.getClusterPositionIndex = situation_id => {
        if(isUndefined(this.cluster_position_lookup[situation_id])) {
            this.cluster_position_lookup[situation_id] = -1;
        }
        this.cluster_position_lookup[situation_id]++;
        return this.cluster_position_lookup[situation_id];
    };


    this.processNewEvent = ev => {
        let is_dupe = this.isDupe(ev);
        let duration = 2500 - (Math.random() * 250);
        let destination = 0.33;
        let cluster_position_index = null;

        if(!is_dupe) {
            duration = 5000 - (Math.random() * 500);
            destination = 0.66;
            this.assignSituationToClusterBucket(ev.situation_id);
            cluster_position_index = this.getClusterPositionIndex(ev.situation_id);
        }

        this.new_event_queue.push({
            is_alive: true,

            ms_start: null,
            duration: duration,
            ms_finish: null,

            destination: destination,
            cluster_position_index: cluster_position_index,

            cluster_duration: 2000,
            ms_cluster_finish: null,

            situation_id: ev.situation_id,
            type: ev.type,

            is_dupe: is_dupe
        });
    };


    this.tick = ms => {
        // Filter out all events that have run their course
        STATE.live_events = STATE.live_events.filter(ev => {
            return ev.is_alive;
        });

        // Initialise new events from the queue
        // NOTE(james): have to initialise here to get context of *when* in ms they start to exist
        let ev;
        while(this.new_event_queue.length > 0) {
            ev = this.new_event_queue.pop();
            ev.ms_start = ms;
            ev.ms_finish = ms + ev.duration;
            if(!ev.is_dupe) {
                ev.ms_cluster_finish = ev.ms_finish + ev.cluster_duration;
            }
            STATE.live_events.push(ev);

            this.updateSituationLastSeen(ev.situation_id, ms);
        }

        // Update live events
        let i, n;
        for(i = 0, n = STATE.live_events.length; i < n; i++) {
            ev = STATE.live_events[i];

            if(ev.ms_finish < ms) {
                if(ev.is_dupe) {
                    ev.is_alive = false;
                }
                else {
                    if(this.situation_last_seen[ev.situation_id] < (ms - MS_MAX_TIME_SINCE_LAST_SEEN)) {
                        ev.is_alive = false;
                    }
                }
            }
        }

        // Update live situations (based on when they were last seen)
        // NOTE(james) have to mutate the array in place in order to retain the observables - assigning a new array with
        // map, filter, or equivalent causes it to stop working.
        STATE.live_situations.splice(0);
        Object.keys(this.situation_last_seen).forEach(situation_id => {
            if(this.situation_last_seen[situation_id] > (ms - MS_MAX_TIME_SINCE_LAST_SEEN)) {
                STATE.live_situations.push(situation_id);
            }
        });
    };


    DataBus.registerEventListener('sim event fired', payload => {
        this.processNewEvent(payload);
    });
}

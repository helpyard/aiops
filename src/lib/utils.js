/**
 * Turn an array of strings into an Enum-like object, e.g. [ 'FOO', 'BAR' ] > { FOO: 0, BAR: 1 }
 * @param {Array} arr
 * @returns {object}
 */
export const enumify = arr => {
    let result = {};
    for(let i = 0, n = arr.length; i < n; i++) {
        result[arr[i]] = i;
    }
    return result;
};


export const isUndefined = v => typeof v === 'undefined';
export const isNullish = v => {
    if(v === null) return true;
    if(v === '') return true;
    return isUndefined(v);
};
export const isZeroish = n => Math.abs(0 - n) < 0.0000000001; // NB to 10th decimal place


export const first = arr => arr[0];
export const last = arr => arr[arr.length - 1];
export const pluckByIndex = (array_of_arrays, i) => array_of_arrays.map(arr => arr[i]);
export const oneAtRandomFrom = arr => {
    const n = arr.length;
    let chosen_index = Math.floor(Math.random() * n);
    if(chosen_index === n) {
        chosen_index = n - 1;
    }
    return arr[chosen_index];
};

export const _2PI = Math.PI * 2;
export const range = (start, end, jump) => {
    if(isUndefined(jump)) jump = 1;
    let current = start;
    let result = [current];
    current += jump;
    while(current <= end) {
        result.push(current);
        current += jump;
    }
    return result;
};
export const min = arr => Math.min.apply(null, arr);
export const max = arr => Math.max.apply(null, arr);
export const sum = arr => {
    let result = 0;
    for(let i = 0, n = arr.length; i < n; i++) {
        result += arr[i];
    }
    return result;
};
export const avg = arr => sum(arr) / arr.length;
export const stdDev = arr => {
    const average = avg(arr);
    let sq_diffs = arr.map(v => Math.pow(v - average, 2));
    return Math.sqrt(avg(sq_diffs));
};

export const clamp = (v, min, max) => {
    if(v > max) {
        return max;
    }
    if(v < min) {
        return min;
    }
    return v;
};
export const lerp = (v0, v1, t) => {
    return v0 * (1-t) + v1 * t;
};

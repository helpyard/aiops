import Vue from '../node_modules/vue/dist/vue.js'
import App from './App.vue'
import {EventBus} from "./lib/EventBus";
import {EventSim} from "./lib/EventSim";
import {SituationManager} from "./lib/SituationManager";



const FRAME_MS_AT_SIXTY_FPS = 1000 / 60;

// NOTE(james): all keys that *could* be in STATE, *should* be in STATE before it gets passed to Vue so that they
// become observable. So.. initialise everything before it gets to Vue, even if it's with null values.
let STATE = {
    ms: 0
};

// Initialise the EventBus with all named events
const DataBus = new EventBus([
    // EventSim
    'sim event fired',

    // UI
    'launch situation clicked'
]);

// Initialise the simulation of Ops events
const OpEventsSim = new EventSim(DataBus);

const SitManager = new SituationManager(DataBus, STATE);

// Main Loop

let request_animation_frame_id;
let prev_ms = 0, prev_frame_duration_ms = 0, ratio_of_ideal_frame_length = 1;

const tickFrame = ms => {
    // calculate frame timing
    prev_frame_duration_ms = ms - prev_ms;
    ratio_of_ideal_frame_length = prev_frame_duration_ms / FRAME_MS_AT_SIXTY_FPS;

    request_animation_frame_id = window.requestAnimationFrame(tickFrame);

    // NB skip the first tick, ie the one before requestAnimationFrame fires for the first time
    if (ms === 0) {
        return;
    }

    DataBus.tick();
    OpEventsSim.tick(ms);
    SitManager.tick(ms);

    STATE.ms = ms;

    // update previous frame time for use next frame
    prev_ms = ms;
};


// ** INIT **


let vm = new Vue({
    el: '#app',
    data: STATE,
    render: h => h(App)
});
vm.$on('go-clicked', () => {
    DataBus.pushEventForFrame('launch situation clicked');
});


// Start the main loop running
tickFrame(0);


// DEBUG

// DataBus.registerEventListener('sim event fired', payload => {
//     console.log(payload);
// });
